import React, { useState } from "react";
import Table from './Table';


export const InputComponent = ({data,information}) => {

    const [newData, setNewData] = useState([]); 

    const nameHandler = (event) =>{
      setNewData({...newData, [event.target.name] : event.target.value })
    }

    const submitHandler = (event) =>{
      event.preventDefault()
      information([...data, {id : data.length + 1, ...newData }])
    }

  return (
    <div>
   
    <form onSubmit={submitHandler}>
    <div>
        <div class="text-5xl text-center pt-10 font-bold">
          <h1 class="bg-gradient-to-r from-blue-600 via-green-500 to-indigo-400 inline-block text-transparent bg-clip-text">Please fill your Information</h1>
        </div>

        <div class=" w-[70%] m-auto mt-4 font-[Barlow]">
          <label
            for="input-group-1"
            class="block mb-2  text-black dark:text-black text-xl  font-bold"
          >
            Username
          </label>
          <div class="relative mb-6">
            <div class="absolute inset-y-0 left-0 flex items-center pl-3 pointer-events-none text-gray-600 font-bold">
              🐼
            </div>
            <input
              type="text"
              class=" text-gray-900 text-sm rounded-md  block w-full pl-10 p-4  dark:bg-white dark:border-gray-600 dark:placeholder-black dark:text-black font-semibold"
              placeholder="Enter your name"
              name="username"
              onChange={nameHandler}
            />
          </div>

          <label
            for="input-group-1"
            class="block mb-2 font-bold text-gray-900 dark:text-black text-xl"
          >
            Age
          </label>
          <div className="relative mb-6">
            <div className="absolute inset-y-0 left-0 flex items-center pl-3 pointer-events-none">
              🔢
            </div>
            <input
              type="number"
              name="age"
              className=" text-gray-900 text-sm rounded-md  block w-full pl-10 p-4  dark:bg-white dark:border-gray-600 dark:placeholder-black dark:text-black font-semibold"
              placeholder="Enter your age"
              onChange={nameHandler}
            />
          </div>

          <div className="text-center">
            <button
              type="submit"
              class="bg-white  p-3 mt-6 w-[250px] rounded-md font-bold text-center bg-gradient-to-r hover:from-gray-400 hover:to-sky-700 border-2 border-blue-500 hover:text-white"
            >
              Submit
            </button>
          </div>
        </div>
        
    </div>
    </form>
    <Table passData = {data}/>
    </div>
  )
}
