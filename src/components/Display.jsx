
import React, { useState } from "react";
import { InputComponent } from "./InputComponent";


export default function Display() {
  const [datas, setData] = useState([
    {
      id: 1,
      username: "Vidijame",
      age: 22,
    },

    {
      id: 2,
      username: "Socheat",
      age: 23,
    },
    {
      id: 3,
      username: "Salin",
      age: 23,
    },
  ]);
  console.log(datas);

  return (
    <div>
      <InputComponent  data={datas} information={setData}/>
    </div>
  );
}
