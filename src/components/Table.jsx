import React from "react";

const Table = ({ passData }) => {
  return (
    // <div>
    //   <table>
    //     <table>
    //       <thead>
    //         <tr>
    //           <th>ID</th>
    //           <th>Username</th>
    //           <th>Age</th>
    //         </tr>
    //       </thead>
    //       <tbody>
    //         {passData.map((item) => (
    //           <tr>
    //             <td>{item.id }</td>
    //             <td>{item.username ? item.username : "null" }</td>
    //             <td>{item.age ? item.age : "null"}</td>
    //           </tr>
    //         ))}
    //       </tbody>
    //     </table>
    //   </table>
    // </div>
    <div class="relative overflow-x-auto mt-12 text-center font-[Barlow]">
    <table class="w-[70%] text-xl text-left text-black dark:text-black m-auto p-4">
        <thead class="text-md text-white dark:bg-slate-500 dark:text-white bg-[#3E54AC]">
            <tr>
                <th scope="col" class="p-5 text-center rounded-l-md ">
                    ID
                </th>
                <th scope="col" class="p-5 text-center">
                   USERNAME
                </th>
                <th scope="col" class="p-5 text-center">
                    AGE
                </th>
            </tr>
        </thead>
        <tbody>
            {/* Display Data */}
                {passData.map((datalist)=>(
                    <tr  class="p-10 h-20 text-md font-bold text-center even:bg-blue-200 odd:bg-slate-200" key={datalist.id}  >
                        <td class="rounded-l-md">{datalist.id}</td>
                        <td>{datalist.username ? datalist.username : `null`}</td>    
                        <td>{datalist.age ? datalist.age : `null`}</td>
                    </tr>
                   
                ))}
        </tbody>
    </table>    
</div>
  );
};

export default Table;
