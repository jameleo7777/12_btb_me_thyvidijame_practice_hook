
import './App.css';
import Display from './components/Display';

// import { InputComponent } from './components/InputComponent';

function App() {
  return (
    <div className="bg-gray-200">
      {/* <InputComponent/> */}
     <Display/>
    </div>
  );
}

export default App;
